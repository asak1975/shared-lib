# README #

This repository contains common JAR files used in SAK framework.

- **basic** Set of core JARs: Spring, Apache Commons, Apache IO, JSOUP etc

- **google** Google's shared libraries

- **jdbc** Set of JDBC drivers

- **mybatis** MyBatis ORM library

- **thumbnailator** Thumbnail creating library

- **zxing** QR code creating library
